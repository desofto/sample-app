require 'rails_helper'

feature 'Leader' do
  before { Timecop.travel Time.zone.parse('01.01.2012 12:00') }
  after { Timecop.return }

  background do
    group1 = create(:group)
    group2 = create(:group, name: 'Brigádníci')

    @user = create(:user)
    @user.leaderships << group1
    login_as @user

    company1 = create(:account, code: 'alaxsro')
    company2 = create(:account, code: 'abcsro')
    work_type1 = create(:work_type, name: 'účtování')
    work_type2 = create(:work_type, name: 'třídění')
    person1 = create(:user, email: 'vomacka@abc.cz')

    create(:work_log,
           user: person1,
           account: company2,
           work_type: create(:work_type, name: 'skenování'),
           started_at: '01.01.2012 12:00',
           duration: 15)

    create(:work_log,
           user: person1,
           account: company1,
           work_type: work_type1,
           started_at: '01.01.2012 13:00',
           duration: 180)

    create(:work_log,
           user: person1,
           account: company2,
           work_type: work_type1,
           started_at: '27.12.2011 12:00',
           duration: 10)

    create(:work_log,
           user: person1,
           account: company1,
           work_type: work_type1,
           started_at: '26.12.2011 12:00',
           duration: 20)

    create(:work_log,
           user: person2 = create(:user, email: 'novak@cnn.com'),
           account: company2,
           work_type: work_type2,
           started_at: '01.01.2012 12:00',
           duration: 185)

    create(:work_log,
           user: person3 = create(:user, email: 'pribyl@alex.cz'),
           account: company1,
           work_type: work_type2,
           started_at: '29.12.2011 12:00',
           duration: 60)

    create(:work_log,
           user: person4 = create(:user, email: 'vonasek@alex.cz'),
           account: company1,
           work_type: work_type2,
           started_at: '29.12.2011 12:00',
           duration: 60)

    person1.memberships << group1
    person2.memberships << group1
    person3.memberships << group1
    person4.memberships << group2

    visit stats_work_logs_path
  end

  scenario 'vidí pouze statistiky členů své skupiny' do
    expect(page).to have_content 'vomacka'
    expect(page).to have_content 'novak'
    expect(page).to have_content 'pribyl'
    expect(page).to_not have_content 'vonasek'
  end

  scenario 'vidí dnešní statistiky', js: true do
    expect(page).to have_content 'Činnosti'

    within('#stats_by_work_type_name') do
      expect(page).to have_row 1, 'třídění'
      expect(page).to have_row 1, '3:05'
      expect(page).to have_row 2, 'účtování'
      expect(page).to have_row 2, '3:00'
      expect(page).to have_row 3, 'skenování'
      expect(page).to have_row 3, '15'
    end

    expect(page).to have_content 'Zákazníci'
    within('#stats_by_account_code') do
      expect(page).to have_row(1, 'abcsro')
      expect(page).to have_row(1, '3:20')
      expect(page).to have_row(2, 'alaxsro')
      expect(page).to have_row(2, '3:00')
    end

    expect(page).to have_content 'Uživatelé'
    within('#stats_by_user_user_name') do
      expect(page).to have_row(1, 'vomacka')
      expect(page).to have_row(1, '3:15')
      expect(page).to have_row(2, 'novak')
      expect(page).to have_row(2, '3:05')
    end

    expect(page).to have_field 'q_started_at_beginning_of_day_gt', with: '01.01.2012'
    expect(page).to have_field 'q_started_at_end_of_day_lt', with: '01.01.2012'
  end

  scenario 'vidí stats u vybrané činnosti a uživatele za posledních sedm dní', js: true do
    select 'účtování', from: 'Činnost'
    select 'vomacka', from: 'Uživatel'
    trigger_click('#work_log_period_custom')
    fill_in 'q_started_at_beginning_of_day_gt', with: '26.12.2011'
    fill_in 'q_started_at_end_of_day_lt', with: '01.01.2012'

    click_button 'Zobrazit'

    expect(page).to have_content 'Činnosti'

    within('#stats_by_work_type_name') do
      expect(page).to have_row(1, 'účtování')
      expect(page).to have_row(1, '3:30')
    end

    expect(page).to have_content 'Zákazníci'
    within('#stats_by_account_code') do
      expect(page).to have_row(1, 'alaxsro')
      expect(page).to have_row(1, '3:20')
      expect(page).to have_row(2, 'abcsro')
      expect(page).to have_row(2, '10')
    end

    expect(page).to have_content 'Uživatelé'
    within('#stats_by_user_user_name') do
      expect(page).to have_row(1, 'vomacka')
      expect(page).to have_row(1, '3:30')
    end

    expect(page).to have_field 'q_started_at_beginning_of_day_gt', with: '26.12.2011'
    expect(page).to have_field 'q_started_at_end_of_day_lt', with: '01.01.2012'
  end
end
