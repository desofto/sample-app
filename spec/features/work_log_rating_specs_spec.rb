require 'rails_helper'

feature 'work_log' do
  background do
    @user = create(:user)
    login_as @user
  end

  scenario 'rating process', js: true do
    company = create(:company, type: :company, name: 'UOL')
    person = create(:person, company: company, email_addresses: [create(:work_email)])
    create(:account, code: 'uol', status: 'initial', main_contact: person,
           tasks: ['create_wiki', 'create_termix_dir'], subject_type: 'company')

    create(:work_type, name: 'non_ratable_activity', ratable: false)
    create(:work_type, name: 'ratable_activity', ratable: true)

    visit work_logs_path

    click_link 'Přidat'

    within('form#new_work_log') do
      select 'uol', from: 'Zákazník'
      select 'non_ratable_activity', from: 'Typ aktivity'

      expect(page).to_not have_selector('#work_log_rating', visible: true)
      expect(page).to_not have_selector('#work_log_rating_comment', visible: true)
      expect(page).to_not have_selector('#work_log_rated_user_id', visible: true)

      select 'ratable_activity', from: 'Typ aktivity'

      fill_in 'Doba trvání', with: 5

      select '3', from: 'Hodocení'
      fill_in 'Hodnotící komentář', with: 'Test comment'
      select @user.to_s, from: 'Hodnocený'

      click_button 'Vytvořit'
    end

    expect(page).to have_attribute('Hodocení', '3')
    expect(page).to have_attribute('Hodnotící komentář', 'Test comment')
    expect(page).to have_attribute('Hodnocený', @user.to_s)
  end
end
