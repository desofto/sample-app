require 'rails_helper'

describe WorkLog do
  TIME_EXCEED_ERROR = 'čas záznamu nesmí přesahovat do druhého dne'.freeze
  TIME_OVERLAP_ERROR = 'již existuje jiný záznam, který se s tímto časově překrývá'.freeze

  %w(user account work_type).each do |model|
    it { expect(subject).to belong_to(model.to_sym) }
  end

  describe '#ended_at' do
    let(:work_log) do
      build(:work_log,
            started_at: '2012-04-04 12:00',
            duration: 70,
            ended_at: nil)
    end

    it 'should be calculated unless is not set' do
      expect(work_log[:ended_at]).to be_nil
      expect(work_log.ended_at).to eq Time.zone.parse('2012-04-04 13:10')
      work_log.duration = 80
      expect(work_log.ended_at).to eq Time.zone.parse('2012-04-04 13:20')
      work_log.duration = nil
      expect(work_log.ended_at).to be_nil
    end

    it 'should be saved' do
      work_log.save
      work_log.reload
      expect(work_log.ended_at).to eq Time.zone.parse('2012-04-04 13:10')
      expect(work_log[:ended_at]).to eq Time.zone.parse('2012-04-04 13:10')
      work_log.started_at = '2012-05-05 16:00'
      work_log.duration = 30
      # po zmene ulozeneho zaznamu by nemela vracet ulozenou, ale vypocitanou hodnotu
      expect(work_log.ended_at).to eq Time.zone.parse('2012-05-05 16:30')
      work_log.save
      work_log.reload
      expect(work_log[:ended_at]).to eq Time.zone.parse('2012-05-05 16:30')
      expect(work_log.ended_at).to eq Time.zone.parse('2012-05-05 16:30')
    end
  end

  context 'validations' do
    %w(user account work_type started_at duration).each do |attr|
      it { expect(subject).to validate_presence_of attr.to_sym }
    end

    context 'duration' do
      it 'should be > 0' do
        work_log = WorkLog.new(duration: 0)
        expect(work_log).to_not be_valid
        expect(work_log.errors[:duration]).to_not be_empty
      end

      it 'should be integer' do
        work_log = WorkLog.new(duration: 1.1)
        expect(work_log).to_not be_valid
        expect(work_log.errors[:duration]).to_not be_empty
      end
    end

    it 'should start and end the same day' do
      work_log = WorkLog.new(started_at: '2012-04-04 12:00')

      work_log.duration = 60 * 2
      work_log.valid?
      expect(work_log.errors[:duration]).to_not include(TIME_EXCEED_ERROR)

      work_log.duration = 60 * 12 - 1
      work_log.valid?
      expect(work_log.errors[:duration]).to_not include(TIME_EXCEED_ERROR)

      work_log.duration = 60 * 12 + 1
      work_log.valid?
      expect(work_log.errors[:duration]).to include(TIME_EXCEED_ERROR)
    end

    describe '#rating' do
      it 'should validatee values' do
        expect(subject).to validate_inclusion_of(:rating).in_range(1..5)
      end
    end
  end

  describe 'after_initialize' do
    before(:each) do
      @time_now = Time.zone.parse('Feb 24 2010')
      Timecop.freeze(@time_now)
    end

    after { Timecop.return }

    it 'should set default started_at unless exists' do
      work_log = WorkLog.new
      expect(work_log.started_at).to eq @time_now
    end

    it 'should not set default started_at if exists' do
      work_log = WorkLog.new(started_at: @time_now + 1)
      expect(work_log.started_at).to eq @time_now + 1
    end

    it 'is invalid when rated without rated_user' do
      work_log = WorkLog.new(rating: 2, rating_comment: 'test')
      work_log.validate
      expect(work_log.errors.messages[:rated_user_id]).to eq ['je povinná položka']
    end
  end

  describe 'started_at=' do
    it 'does not assign value to attribute if the value is not valid' do
      work_log = WorkLog.new(started_at: '987')
      expect(work_log.started_at).to be_nil
      work_log.started_at = '01.01.2010 00:00123'
      expect(work_log.started_at).to be_nil
    end
  end
end
