require 'rails_helper'

describe PhoneCall do
  describe '#create_work_log' do
    Phones = Struct.new(:company_phone, :person_phone, :employee_phone)

    before do
      @user = create(:user, email: 'bohuslav.blin@uol.cz')
      @person = create(:person, last_name: 'Blín')
      @person_phone = create(:phone_number, contact: @person, number: '272047021')
      create(:email_address, contact: @person, address: 'bohuslav.blin@uol.cz')

      @company = create(:company, :ready_for_account)
      @account = create(:account, company: @company)
      @company_phone = create(:phone_number, contact: @company, number: '222111111')
      @employee = create(:person, company: @company)
      @employee_phone = create(:phone_number, contact: @employee, number: '777123456')

      @work_type = create(:work_type, name: 'Telefonování')
    end

    [
      {
        result_type: 'valid',
        context: 'INCOMING call from COMPANY',
        call_type: 'in',
        caller: -> (phones) { phones.company_phone.number },
        called: -> (phones) { phones.person_phone.number }
      },
      {
        result_type: 'valid',
        context: 'OUTGOING call to COMPANY',
        call_type: 'out',
        caller: -> (phones) { phones.person_phone.number },
        called: -> (phones) { phones.company_phone.number }
      },
      {
        result_type: 'valid',
        context: 'OUTGOING call to PERSON in COMPANY',
        call_type: 'out',
        caller: -> (phones) { phones.person_phone.number },
        called: -> (phones) { phones.employee_phone.number }
      },
      {
        result_type: 'valid',
        context: 'INCOMING call from PERSON in COMPANY',
        call_type: 'in',
        caller: -> (phones) { phones.employee_phone.number },
        called: -> (phones) { phones.person_phone.number }
      },
      {
        result_type: 'invalid',
        context: 'INCOMING call from UNKNOWN',
        call_type: 'in',
        caller: -> (_phones) { '155' },
        called: -> (_phones) { '112' }
      }
    ].each do |scenario|
      describe 'User which had ' + scenario[:context] do
        before do
          phones = Phones.new(@company_phone, @person_phone, @employee_phone)
          phone_call_params = {
            call_type: scenario[:call_type],
            caller_number: scenario[:caller].call(phones),
            called_number: scenario[:called].call(phones),
            duration: 1, started_at: Time.zone.now
          }
          @phone_call = PhoneCall.build(phone_call_params)
        end

        case scenario[:result_type]
        when 'valid'
          it 'should be valid' do
            expect(@phone_call).to be_valid
          end
          it 'builds valid work_log' do
            work_log = @phone_call.build_work_log
            expect(work_log).to be_valid
            expect(@account).to eq work_log.account
            expect(@user).to eq work_log.user
            expect(@work_type).to eq work_log.work_type
            case scenario[:call_type]
            when 'in'
              expect("Příchozí hovor z čísla #{@phone_call.caller_number}")
                .to eq work_log.comment
            when 'out'
              expect("Odchozí hovor na číslo #{@phone_call.called_number}")
                .to eq work_log.comment
            end
          end
        when 'invalid'
          it 'should be invalid' do
            expect(@phone_call).to_not be_valid
            expect(@phone_call).to have(1).error_on(:caller_number)
            expect(@phone_call).to have(1).error_on(:called_number)
            expect(@phone_call.errors.count).to eq 2
          end
          it 'raises exception on build work_log' do
            expect do
              @phone_call.build_work_log
            end.to raise_error ActiveRecord::RecordInvalid
          end
        end
      end
    end
  end
end
