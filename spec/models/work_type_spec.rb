require 'rails_helper'

describe WorkType do
  it { expect(subject).to have_many(:work_logs).dependent(:restrict_with_exception) }

  it { expect(subject).to validate_presence_of(:name) }
  it { expect(subject).to validate_length_of(:name).is_at_least(3).is_at_most(20) }

  describe '#name' do
    before { create(:work_type) }
    it { expect(subject).to validate_uniqueness_of(:name) }
  end

  describe '#deletable?' do
    it { expect(subject).to be_deletable }

    describe 'with work_log' do
      before { subject.work_logs << WorkLog.new }
      it { expect(subject).to_not be_deletable }
    end
  end

  describe 'default scope' do
    before do
      @w1 = create(:work_type, name: 'ccc')
      @w2 = create(:work_type, name: 'aaa')
      @w3 = create(:work_type, name: 'bbb')
    end

    it 'should order by name' do
      expect(WorkType.all).to eq [@w2, @w3, @w1]
    end
  end
end
