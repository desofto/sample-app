require 'rails_helper'

def all_tasks
  [:terminate_bank_statements, :activate_eso9, :hand_over_accounting,
   :file_tax_return_and_terminate_power_of_attorney, :close,
   :close_accounting]
end

describe TaxReturnContractPolicy do
  let(:contract) { TaxReturnContract.new }
  let(:policy) { TaxReturnContractPolicy.new(user, contract) }

  let(:user_permissions) do
    [
      :starts_on,
      :ends_on,
      :state,
      :account_id,
      :note,
      tasks: []
    ]
  end
  let(:admin_permissions) { user_permissions }
  let(:account_manager_permissions) { admin_permissions }
  let(:bank_statements_manager_permissions) { user_permissions }
  let(:accountant_leader_permissions) { user_permissions }
  let(:payroll_accountant_leader_permissions) { user_permissions }
  let(:shipping_manager_permissions) { user_permissions }

  context 'for user' do
    let(:user) { create(:user) }
    tasks = []

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_falsey }
    it { expect(policy.update?).to be_falsey }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array user_permissions
    end
  end

  context 'for accountant' do
    let(:user) { create(:user) }
    tasks = [:close_accounting]
    let(:contract) do
      AccountingContract.new(data: AccountingContractData.new(accountant: user))
    end

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_falsey }
    it { expect(policy.update?).to be_falsey }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array user_permissions
    end
  end

  context 'for admin' do
    let(:user) { create(:admin) }
    tasks = []

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_truthy }
    it { expect(policy.update?).to be_truthy }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array admin_permissions
    end
  end

  context 'for account manager' do
    let(:user) { create(:account_manager) }
    tasks = [:activate_eso9, :close]

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_truthy }
    it { expect(policy.update?).to be_truthy }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array account_manager_permissions
    end
  end

  context 'for bank statements manager' do
    let(:user) { create(:bank_statements_manager) }
    tasks = [:terminate_bank_statements]

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_falsey }
    it { expect(policy.update?).to be_falsey }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes)
        .to match_array bank_statements_manager_permissions
    end
  end

  context 'for accountant leader' do
    let(:user) { create(:accountant_leader) }
    tasks = []

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_falsey }
    it { expect(policy.update?).to be_falsey }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array accountant_leader_permissions
    end
  end

  context 'for payroll accountant leader' do
    let(:user) { create(:payroll_accountant_leader) }
    tasks = []

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_falsey }
    it { expect(policy.update?).to be_falsey }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes)
        .to match_array payroll_accountant_leader_permissions
    end
  end

  context 'for shipping manager' do
    let(:user) { create(:shipping_manager) }
    tasks = []

    it { expect(policy.index?).to be_truthy }
    it { expect(policy.show?).to be_truthy }
    it { expect(policy.edit?).to be_truthy }
    it { expect(policy.update?).to be_truthy }
    (all_tasks - tasks).each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_falsey
      end
    end
    tasks.each do |task|
      it "#run_task?(#{task})" do
        expect(policy.run_task?(task)).to be_truthy
      end
    end

    it 'provides correct permissions' do
      expect(policy.permitted_attributes).to match_array shipping_manager_permissions
    end
  end
end
