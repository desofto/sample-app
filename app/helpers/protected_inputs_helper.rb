module ProtectedInputsHelper
  def restricted(f, field)
    case f.object
    when Contact, Person, Company
      policy = policy(f.object)
      return unless policy.permitted_attribute?(field) ||
                    policy.permitted_attribute?("#{field}_id") ||
                    policy.permitted_attribute?("#{field}_id".to_sym)
    end
    yield
  end

  def restricted_input(f, method, options = {})
    restricted f, method do
      f.input method, options
    end
  end
end
