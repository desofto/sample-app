class ApplicationController < ActionController::Base
  include Pundit
  include ResourceAccessControl

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  API_FORMATS = ['csv'].freeze
  protect_from_forgery with: :exception
  # TODO: Remove after implementing update WorkLog via Grape API
  skip_before_action :verify_authenticity_token, if: :json_request?

  before_action :authenticate_user!
  before_action :set_raven_context
  before_action :restrict_localnet_users
  before_action :restrict_api_only_user
  before_action :set_paper_trail_whodunnit
  before_action :set_carmen_locale
  rescue_from Pundit::NotAuthorizedError, with: :access_denied

  protected

  def set_carmen_locale
    Carmen.i18n_backend.locale = I18n.locale
  end

  def json_request?
    request.format.json?
  end

  def restrict_localnet_users
    return unless current_user.try(:localnet_only) && IPControl.external_net?(request.ip)
    flash.alert = t('flash.users.remote_acces_forbidden')
    sign_out current_user
    redirect_to new_user_session_path
  end

  def restrict_api_only_user
    return unless current_user.try(:api_only) && API_FORMATS.exclude?(request.format)
    flash.alert = t('flash.users.api_interaction_only')
    sign_out current_user
    render text: '401 - Unauthorized request', layout: 'devise_layout', status: 401
  end

  def access_denied
    flash.alert = 'Nemáte oprávnění pro přístup k této stránce.'
    redirect_to root_url
  end

  def store_location
    session[:return_to] = request.request_uri
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def set_raven_context
    return unless user_signed_in?
    Raven.user_context(
      user_id: current_user.id,
      email: current_user.email
    )
  end
end
