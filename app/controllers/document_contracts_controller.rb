class DocumentContractsController < ContractsController
  def create
    contract =  DocumentContract.new(contract_params)
    @contract = Contracts::DocumentContractCreator.new(contract).create
    set_contacts

    respond_with(@contract)
  end

  def show
    @contract = contract_sti.find(params[:id]).decorate
    if params[:show_exceptions]
      @pickup_exceptions = PickupException.expired.for_contract(@contract)
                           .reorder(starts_on: :desc).includes(:pickup)
    end

    @versions = ContractVersion.contract_data(@contract)
    respond_with(@contract)
  end

  def update
    @contract.update(contract_params)

    if @contract.try(:pickups_with_shipments_changed?)
      flash[:alert] = t('flash.pickups.pickup_with_shipments_changed')
    end

    respond_with(@contract)
  end
end
