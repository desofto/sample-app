class ContractsController < ApplicationController
  before_action :set_contract_type
  before_action :load_resource, except: [:new, :create]
  before_action :authorize_resource
  decorates_assigned :contract
  before_action :load_contract, only: [:edit, :update]
  before_action :set_contacts, only: [:edit, :update]
  before_action :set_accountants,
    :set_payroll_accountants,
    :set_tax_advisors,
    only: [:index, :new, :create, :edit, :update]

  responders :flash
  respond_to :html

  helper_method :search_params

  def index
    @search = search_class.new(contract_sti, search_params)
    @search.type = params[:type]
    @search.scope = params[:scope]
    @contracts = @search.result.order(created_at: :desc)

    respond_with(@contracts) do |format|
      format.csv do
        @filename = 'kontrakty.csv'
        @output_encoding = 'UTF-8'
      end
      format.html do
        @contracts = @contracts.page(params[:page])
      end
    end
  end

  def show
    @contract = contract_sti.find(params[:id]).decorate
    @versions = ContractVersion.contract_data(@contract)
    respond_with(@contract)
  end

  def new
    @contract = contract_sti.new(account_id: params[:account_id])
    set_contacts
  end

  def edit
    respond_with(@contract)
  end

  def create
    @contract = contract_sti.new(contract_params)
    @contract.save
    set_contacts
    respond_with(@contract)
  end

  def update
    @contract.update(contract_params)
    respond_with(@contract)
  end

  def destroy
    @contract.destroy
    respond_with(@contract)
  end

  def run_task
    task_params.each do |task|
      @contract.send(task)
    end

    respond_to do |format|
      format.html { redirect_to contract_path(@contract) }
      format.json { render json: { success: 'OK' } }
    end
  end

  protected

  def load_contract
    @contract = contract_sti.find(params[:id])
  end

  def contract_params
    set_contract_type
    key = @contract_type.underscore.to_sym
    if params[key]
      params[key][:tasks] ||= [] if params[key].key?(:tasks)
      params.require(key).permit(policy(@contract || contract_sti).permitted_attributes)
    else
      {}
    end
  end

  def search_class
    "Search::#{@contract_type}".constantize
  rescue NameError
    Search::Contract
  end

  def search_params
    params.fetch(:contracts_search, {}).merge(params.permit([:code, :user_name]))
  end

  def clazz
    controller_name.singularize
  end

  def set_contract_type
    @contract_type = clazz.camelize
  end

  def contract_sti
    @contract_type.constantize
  end

  def set_contacts
    @contacts = @contract.account.company.people
                .reject { |p| p.available_email_addresses.blank? }
  end

  def current_ability
    tasks = contract_params[:tasks]
    @current_ability ||= Ability.new(current_user, tasks)
  end

  def set_payroll_accountants
    @payroll_accountants = User.with_role(:payroll_accountant).active
  end

  def set_accountants
    @accountants = User.with_role(:accountant).active
  end

  def set_tax_advisors
    @tax_advisors = User.with_role(:tax_advisor).active
  end

  private

  def task_params
    permitted_tasks & contract_params[:tasks]
  end

  def permitted_tasks
    ContractPolicy.new(current_user, nil).send(:group_tasks).keys.map(&:to_s) +
      %w(close_accounting close_payroll_accounting close)
  end
end
