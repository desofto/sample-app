module API
  module V1
    class Account < Grape::API
      resource :accounts do
        paginate
        params do
          optional :code, type: String
          optional :task, type: String
        end
        # TODO: Current problem is, that it returns all records for uknown params
        # For next API ver, create uri like /search/accounts where it will
        # always expect params and it returns empty list for uknown params
        get do
          authorize ::Account, :index?
          result = ::Account.all.order(:code)
          result = result.where(code: params[:code]) if params[:code].present?
          result = result.where('? = ANY(tasks)', params[:task]) if params[:task].present?
          present paginate(result), root: 'accounts'
          present :meta, { href: request.url }, {}
        end

        params do
          requires :id, type: Integer
        end
        route_param :id do
          get do
            account = ::Account.find_by_id!(params[:id])
            authorize account, :show?
            present account
          end

          params do
            requires :task, type: String
          end
          put '/task_completed' do
            account = ::Account.find_by_id!(params[:id])
            authorize account, :can_manage?
            ::AccountTaskCompleter.task_completed(account, params[:task])
          end

          # TODO: Deprecated, remove this method. Use task_completed
          params do
            requires :task, type: String
          end
          patch '/run_task' do
            account = ::Account.find_by_id!(params[:id])
            authorize account, :run_task?
            account.send(params[:task])
          end
        end
      end
    end
  end
end
