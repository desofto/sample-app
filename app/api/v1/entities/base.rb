module API
  module V1
    module Entities
      class Base < Grape::Entity
        include API::V1::UrlHelpers

        def self.expose_collection_association(association_name, url_method)
          expose association_name do |_resource|
            { meta: { href: send(url_method, request_host, object.id) } }
          end
        end

        def self.expose_singular_association(association_name, url_method)
          expose association_name do |resource|
            if resource.send("#{association_name}_id").present?
              {
                meta: {
                  href: send(url_method, request_host,
                             resource.send("#{association_name}_id"))
                }
              }
            end
          end
        end

        format_with(:iso_timestamp, &:iso8601)

        expose :id

        with_options(format_with: :iso_timestamp) do
          expose :created_at
          expose :updated_at
        end

        expose :meta do |resource|
          resource_name = self.class.to_s.demodulize.underscore
          resource_url_name = "#{resource_name}_url"
          { href: send(resource_url_name, resource) } if respond_to?(resource_url_name)
        end

        def request_host
          options[:env]['HTTP_HOST']
        end
      end
    end
  end
end
