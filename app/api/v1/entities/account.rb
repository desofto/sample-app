module API
  module V1
    module Entities
      class Account < Base
        expose :code
        expose :company_id
        expose :company
        expose :main_contact_id
        expose :tasks
        expose :subject_type
        expose :referred_by_id
        expose :referral_comment
        expose :status
      end
    end
  end
end
