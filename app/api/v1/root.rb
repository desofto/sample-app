module API
  module V1
    class Root < Grape::API
      include API::Exceptions

      helpers Pundit
      helpers do
        include API::V1::UrlHelpers
        attr_reader :current_user
      end

      helpers do
        def declared_params
          ActionController::Parameters.new(declared(params, include_missing: false))
        end
      end

      http_basic do |email, password|
        @current_user = ::User.find_by_email(email)
        next unless @current_user && @current_user.valid_password?(password)
        @current_user
      end

      version 'v1', using: :path
      format :json

      represent ::Account,                with: API::V1::Entities::Account
      represent ::Contact,                with: API::V1::Entities::Contact
      represent ::Company,                with: API::V1::Entities::Company
      represent ::Person,                 with: API::V1::Entities::Person
      represent ::Contract,               with: API::V1::Entities::Contract
      represent ::FantozziContract,       with: API::V1::Entities::FantozziContract
      represent ::PayrollContract,        with: API::V1::Entities::PayrollContract
      represent ::User,                   with: API::V1::Entities::User
      represent ::BonoboAccessPermission, with: API::V1::Entities::BonoboAccessPermission

      mount API::V1::Account
      mount API::V1::Contact
      mount API::V1::Company
      mount API::V1::Person
      mount API::V1::Contract
      mount API::V1::FantozziContract
      mount API::V1::PayrollContract
      mount API::V1::User
      mount API::V1::BonoboAccessPermission
      mount API::V1::PhoneCall
    end

    class Public < Grape::API
      version 'v1', using: :path
      format :json

      mount API::V1::Ping
    end
  end
end
