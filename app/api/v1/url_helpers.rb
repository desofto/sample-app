module API
  module V1
    module UrlHelpers
      def api_url
        schema   = 'https://'
        host     = options[:env]['HTTP_HOST']
        schema + host + '/api/v1'
      end

      def account_url(account)
        api_url + "/accounts/#{account.id}"
      end

      def contact_url(contact)
        api_url + "/contacts/#{contact.id}"
      end

      def company_url(company)
        api_url + "/companies/#{company.id}"
      end

      def person_url(person)
        api_url + "/people/#{person.id}"
      end

      def contract_url(contract)
        api_url + "/contracts/#{contract.id}"
      end

      def accounting_contract_url(contract)
        api_url + "/accounting_contracts/#{contract.id}"
      end

      def fantozzi_contract_url(contract)
        api_url + "/fantozzi_contracts/#{contract.id}"
      end

      def payroll_contract_url(contract)
        api_url + "/payroll_contracts/#{contract.id}"
      end

      def user_url(user)
        api_url + "/users/#{user.id}"
      end

      def bonobo_access_permission_url(bonobo_access_permission)
        api_url + "/bonobo_access_permissions/#{bonobo_access_permission.id}"
      end
    end
  end
end
