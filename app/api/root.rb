module API
  class Root < Grape::API
    mount API::V1::Root
    mount API::V1::Public
  end
end
