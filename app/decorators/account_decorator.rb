class AccountDecorator < Draper::Decorator
  include ActionView::Helpers::TranslationHelper
  delegate_all

  def status_to_s
    return unless status
    t(status, scope: 'activerecord.attributes.account.statuses')
  end

  def reference_to_s
    return unless reference
    t(reference, scope: 'activerecord.attributes.account.reference_types')
  end

  def link_to_new_contract(contract)
    contract = "#{contract.to_s.camelcase}Contract".constantize
    title = I18n.t(contract, scope: 'activerecord.attributes.contract.type_values')
    h.link_to title, h.new_polymorphic_path([self, contract])
  end
end
