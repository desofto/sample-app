class WorkLogDecorator < Draper::Decorator
  delegate_all

  def rating_with_comment
    comment = []
    comment << rating.to_s unless rating.nil?
    comment << rating_comment unless rating.nil? || rating_comment.blank?
    comment.join(' - ')
  end
end
