class User < ActiveRecord::Base
  devise :database_authenticatable, :rememberable, :trackable,
    :validatable, :lockable, :omniauthable

  before_save :ensure_authentication_token
  after_save :flush_cache
  after_destroy :flush_cache

  with_options dependent: :restrict_with_exception do |user|
    user.has_many :work_logs
    user.has_many :requested_shipments, foreign_key: 'applicant_id'
    user.has_many :incoming_shipments,  foreign_key: 'recipient_id'
  end

  has_many :group_members
  has_many :memberships, through: :group_members, source: :group,
    after_remove: :drop_leadership
  has_many :group_leaders
  has_many :leaderships, through: :group_leaders, source: :group,
    after_remove: :revoke_bonobo_access_permission_if_not_accountant

  before_destroy :revoke_bonobo_access_permission

  has_many :ratings, foreign_key: :rated_user_id, class_name: WorkLog
  has_many :leaders, through: :memberships, source: :leaders

  scope :active, -> { where(closed: false) }
  scope :closed, -> { where(closed: true) }
  scope :human, -> { where(api_only: false) }
  scope :with_work_logs, -> { joins(:work_logs).select('DISTINCT users.*') }
  scope :shipment_applicants, lambda {
    joins(:requested_shipments).select('DISTINCT users.*')
  }
  scope :in_groups, ->(groups) { groups.map(&:members).flatten.uniq }
  scope :with_role, (lambda do |role|
    joins(:memberships).where('groups.roles @> ?', "{#{role}}")
  end)

  scope :without_group, (lambda do
    joins('left join groups_members on groups_members.user_id=users.id')
    .where(groups_members: { id: nil })
  end)

  default_scope { order(:email) }

  # HACK: prepsana metoda Devise kvuli fetchovani usera z cache
  # https://github.com/plataformatec/devise/blob/3d9dea39b2978e3168604ccda956fb6ec17c5e27/
  # lib/devise/models/authenticatable.rb#L207
  def self.serialize_from_session(key, salt)
    single_key = key.is_a?(Array) ? key.first : key
    user = cached_find(single_key)
    user if user.try(:authenticatable_salt) == salt
  end

  def self.cached_active_human
    Rails.cache.fetch([name, 'active_human']) { active.human.to_a }
  end

  def self.cached_find(id)
    Rails.cache.fetch([self, id]) { find_by(id: id) }
  end

  def self.cached_shipment_applicants
    Rails.cache.fetch([name, 'shipment_applicants']) { shipment_applicants.to_a }
  end

  def self.cached_users_and_groups_list(current_user)
    Rails.cache.fetch(
      [self.class.name, 'cached_users_and_groups_list', current_user.id]
    ) do
      list = []
      current_user.leaderships.each do |group|
        list << ["g-#{group.id}", "<#{group.name}>"]
        group.members.each do |user|
          list << ["u-#{user.id}", user.user_name]
        end
      end
      list
    end
  end

  def self.drop_cached_users_and_groups_list
    Rails.cache.delete_matched(
      [self.class.name, 'cached_users_and_groups_list'].join('/')
    )
  end

  def to_s
    display_name
  end

  def display_name
    user_name
  end

  def user_name
    email.split('@')[0]
  end

  def roles
    memberships.flat_map(&:roles).uniq + leader_roles
  end

  def leader_roles
    leaderships.flat_map(&:roles).map { |r| r + '_leader' }.uniq
  end

  def role?(role)
    roles.include?(role.to_s)
  end

  def group_leader?
    leaderships.any?
  end

  def leader_of_group_with_role?(role)
    leaderships.flat_map(&:roles).uniq.include?(role.to_s)
  end

  def leader_of_user?(user_id)
    (User.find(user_id).memberships & leaderships).any?
  end

  def leader_of_users
    User.in_groups(leaderships)
  end

  def active_for_authentication?
    super && closed.blank?
  end

  def deactivate
    update(closed: true)
    memberships.clear
    leaderships.clear
  end

  def accountant_leader?
    leaderships.with_role('accountant').any?
  end

  private

  def revoke_bonobo_access_permission_if_not_accountant(_)
    return if accountant_leader?
    revoke_bonobo_access_permission
  end

  def revoke_bonobo_access_permission
    BonoboAccessPermission.revoke(user_id: id)
  end

  def drop_leadership(group)
    group_leaders.where(group_id: group.id).destroy_all
  end

  # authentication_token se pouziva na contacts/versions.atom
  #   kam pristupuje s tokenem externi program a stahuje novinky
  def ensure_authentication_token
    return unless authentication_token.blank?
    self.authentication_token = generate_authentication_token
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.find_by(authentication_token: token)
    end
  end

  def flush_cache
    User.drop_cached_users_and_groups_list
    Rails.cache.delete([self.class.name, id])
    Rails.cache.delete([self.class.name, 'active_human'])
  end
end
