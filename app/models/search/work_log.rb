module Search
  class WorkLog
    include ApplicationHelper
    include ActiveModel::Model

    attr_accessor :user_or_group_id_eq,
      :user_id_in,
      :account_id_eq,
      :work_type_id_eq,
      :work_log_period,
      :started_at_beginning_of_day_gt,
      :started_at_end_of_day_lt

    def initialize(current_user, params)
      super(params)
      @current_user = current_user
      @started_at_beginning_of_day_gt ||= Time.zone.today
      @started_at_end_of_day_lt ||= Time.zone.today
    end

    def result
      @query = ::WorkLog.all
      set_user_filter
      by_user_id_eq
      by_group_id_eq
      by_user_id_in
      by_account_id_eq
      by_work_type_id_eq
      by_started_at_beginning_of_day_gt
      by_started_at_end_of_day_lt
      @query
    end

    private

    def set_user_filter
      if @current_user.group_leader?
        within_subordinates_and_closed_users
      else
        self.user_id_eq = @current_user.id
      end
    end

    def within_subordinates_and_closed_users
      return if user_id_eq.present? && @current_user.leader_of_user?(user_id_eq)
      @user_id_in = @current_user.leader_of_users.map(&:id) +
                    ::User.without_group.map(&:id)
    end

    def by_user_id_eq
      @query = @query.where(user_id: user_id_eq) if user_id_eq.present?
    end

    def by_group_id_eq
      return if group_id_eq.blank?
      @query = @query.joins(user: [:group_members])
               .where(groups_members: { group_id: group_id_eq })
    end

    def by_user_id_in
      return if user_id_in.blank?
      @query = @query.where(work_logs: { user_id: user_id_in })
    end

    def by_account_id_eq
      return if account_id_eq.blank?
      @query = @query.where(account_id: account_id_eq)
    end

    def by_work_type_id_eq
      return if work_type_id_eq.blank?
      @query = @query.where(work_type_id: work_type_id_eq)
    end

    def by_started_at_beginning_of_day_gt
      return if started_at_beginning_of_day_gt.blank?
      started_at = ::WorkLog.arel_table[:started_at]
      @query = @query.where(
        started_at.gteq(started_at_beginning_of_day_gt.to_date.beginning_of_day)
      )
    end

    def by_started_at_end_of_day_lt
      return if started_at_end_of_day_lt.blank?
      started_at = ::WorkLog.arel_table[:started_at]
      @query = @query.where(
        started_at.lteq(started_at_end_of_day_lt.to_date.end_of_day)
      )
    end

    def user_id_eq
      return if user_or_group_id_eq.blank?
      return unless user_or_group_id_eq[0..1] == 'u-'
      user_or_group_id_eq[2..-1]
    end

    def user_id_eq=(id)
      @user_or_group_id_eq = "u-#{id}"
    end

    def group_id_eq
      return if user_or_group_id_eq.blank?
      return unless user_or_group_id_eq[0..1] == 'g-'
      user_or_group_id_eq[2..-1]
    end

    def group_id_eq=(id)
      @user_or_group_id_eq = "g-#{id}"
    end
  end
end
