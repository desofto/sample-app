class BaseMailer < ActionMailer::Base
  after_action :log_mail

  protected

  def log_mail
    Rails.logger.info do
      recipients = Array(mail[:to]).join(', ')
      now = Time.try(:new_without_mock_time) || Time.zone.now
      "\n#{now} \"#{mail[:subject]}\""\
        " Sent mail to #{recipients}"
    end
  end
end
