class BaseContractMailer < BaseMailer
  add_template_helper(AccountsHelper)

  protected

  def set_tag_activate
    @tag = I18n.t('contract_mailer.activate')
  end

  def set_tag_deactivate
    @tag = I18n.t('contract_mailer.deactivate')
  end

  def set_subject
    if @contract
      prefix = "#{@contract.account.code} [Kontrakt##{@contract.id}]"
      prefix = "[#{@tag}] #{prefix}" if @tag
      text = default_i18n_subject(account_name: @contract.account.company_name)
    else
      prefix = '[Kontrakty]'
      prefix = "[#{@tag}] #{prefix}" if @tag
      text = default_i18n_subject
    end
    mail.subject = [prefix, text].join(' - ')
  end
end
