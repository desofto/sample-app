class TaxReturnContractMailer < BaseContractMailer
  before_action :set_tag_activate,
    only: [:add_fantozzi_contract_to_acmanager]
  before_action :set_tag_deactivate,
    only: []

  def add_fantozzi_contract_to_acmanager(contract)
    @contract = contract.decorate
    mail(to: 'acmanager@uol.cz',
         subject: default_i18n_subject(account_code: @contract.account.code))
  end
end
