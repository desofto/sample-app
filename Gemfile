source 'https://rubygems.org'

gem 'rails', '4.2.5.1'
gem 'pg'

gem 'sentry-raven'
# uncomment if you need some performance stats
# gem 'skylight'

# Assets
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'therubyracer', '~> 0.12.2'
gem 'execjs', '~> 2.6.0'
gem 'font-awesome-rails', '~> 4.4.0.0'

# API
gem 'jbuilder', '~> 2.0'
gem 'gibbon'
gem 'grape'
gem 'grape-entity'
# We can use gem when it's version is updated. For now we need bug fix which is only in
#   github master branch
#   the bug is in links to next/last pages in headers
gem 'api-pagination', git: 'https://github.com/davidcelis/api-pagination.git',
  ref: 'a70ac6f'
gem 'json-schema'

# ActiveJob
gem 'sinatra', require: nil
gem 'sidekiq', '~> 3.5.1'

# Auth and audit
gem 'devise'
gem 'pundit'
gem 'omniauth-google-oauth2'

gem 'paper_trail'
gem 'geocoder'

# Model
gem 'acts-as-taggable-on'
gem 'state_machine'
gem 'holidays'
# Use rubygems when updated to 1.0.3
gem 'carmen', git: 'https://github.com/jim/carmen'
# gem 'validates_overlap'
gem 'ico-validator'
gem 'ares.rb'

# Controller
gem 'responders'
gem 'pg_search'
gem 'kaminari'

# Views
gem 'formtastic'
gem 'show_for'
gem 'csv_builder'
gem 'chosen-rails'
gem 'sprockets-rails', '< 3.0'
# https://github.com/tsechingho/chosen-rails/issues/70
# get rid of "uninitialized constant Sprockets::SassCacheStore"
gem 'compass-rails', git: 'https://github.com/Compass/compass-rails.git', branch: 'master'
gem 'draper'

# JS
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'rails3-jquery-autocomplete'

# Cache
gem 'redis-rails'

# PDF
gem 'prawn-labels'

# Database
gem 'pghero'

# xlsx
gem 'rubyXL'

group :production do
  gem 'newrelic_rpm'
end

group :development do
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-sidekiq'
  gem 'spring'
  # gem 'spring-commands-rspec'
  gem 'better_errors'
  gem 'bullet'
  gem 'slackistrano', require: false
  gem 'overcommit', require: false
  gem 'rubocop', require: false
  gem 'quiet_assets'
  # pokud je zapnuto zobrazuje v dev modu casy nacitani primo v prohlizeci
  gem 'rack-mini-profiler'
end

group :test, :development do
  gem 'pry', require: false
  gem 'pry-rails', require: false
  gem 'pry-byebug', require: false
  gem 'launchy'
  gem 'rspec-rails'
  gem 'rspec-collection_matchers'
  gem 'rspec-activemodel-mocks'
  gem 'railroady'
  gem 'parallel_tests'
end

group :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'shoulda-matchers', require: false
  gem 'timecop'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'simplecov', require: false
  gem 'vcr'
  gem 'webmock'
end
